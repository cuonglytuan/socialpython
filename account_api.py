"""Hello World API implemented using Google Cloud Endpoints.

Defined here are the ProtoRPC messages needed to define Schemas for methods
as well as those methods defined in an API.
"""

import endpoints
from protorpc import messages
from protorpc import message_types
from protorpc import remote

from google.appengine.ext.webapp import template
from google.appengine.ext import ndb

from datetime import datetime
from datamodels.usermodels import UserModels
from datamodels.usermodels import UserToken

import logging
import os.path
import webapp2
import urllib

from webapp2_extras import auth
from webapp2_extras import sessions

from webapp2_extras.auth import InvalidAuthIdError
from webapp2_extras.auth import InvalidPasswordError

from webapp2_extras import security
from google.appengine.api import users

from google.appengine.ext import blobstore
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

WEB_CLIENT_ID = 'replace this with your web client application ID'
ANDROID_CLIENT_ID = 'replace this with your Android client ID'
IOS_CLIENT_ID = 'replace this with your iOS client ID'
ANDROID_AUDIENCE = WEB_CLIENT_ID

package = 'Hello'

TIME_FORMAT_STRING = '%b %d, %Y %I:%M:%S %p'


def get_endpoints_current_user(raise_unauthorized=True):
    """Returns a current user and (optionally) causes an HTTP 401 if no user.

    Args:
        raise_unauthorized: Boolean; defaults to True. If True, this method
            raises an exception which causes an HTTP 401 Unauthorized to be
            returned with the request.

    Returns:
        The signed in user if there is one, else None if there is no signed in
        user and raise_unauthorized is False.
    """
    current_user = endpoints.get_current_user()
    if raise_unauthorized and current_user is None:
        raise endpoints.UnauthorizedException('Invalid token.')
    return current_user


class UploadHandler(blobstore_handlers.BlobstoreUploadHandler):
    def post(self):
        logging.info('start upload handle')
        upload_files = self.get_uploads('file')
        blob_info = upload_files[0]
        self.redirect('/serve/%s' % blob_info.key())

class Userpost(messages.Message):
    """User sign up informations."""
    email = messages.StringField(1, required=True)
    password = messages.StringField(2, required=True)
    fullname = messages.StringField(3, required=True)
    birthday = messages.StringField(4, required=True)
    mobile = messages.StringField(5)
    address = messages.StringField(6)


class UserpostLogin(messages.Message):
    """User sign up informations."""
    email = messages.StringField(1, required=True)
    password = messages.StringField(2, required=True)

class SignUpResponse(messages.Message):
    """Greeting that stores a message."""
    status = messages.StringField(1)
    userid = messages.IntegerField(2)
    token = messages.StringField(3)
    fullname = messages.StringField(4)


class URLResponse(messages.Message):
    """Greeting that stores a message."""
    url = messages.StringField(1)

@endpoints.api(name='account', version='v1',
               allowed_client_ids=[WEB_CLIENT_ID, ANDROID_CLIENT_ID,
                                   IOS_CLIENT_ID, endpoints.API_EXPLORER_CLIENT_ID],
               audiences=[ANDROID_AUDIENCE],
               scopes=[endpoints.EMAIL_SCOPE])
class AccountApi(remote.Service):
    """account API v1."""

    """ API create account. """
    ACCOUNT_SIGN_UP_RESOURCE = endpoints.ResourceContainer(
            Userpost,
            id=messages.IntegerField(1, variant=messages.Variant.INT32))

    @endpoints.method(Userpost, SignUpResponse,
                      path='account/created', http_method='POST',
                      name='account.created')
    def account_created(self, request):
        try:
            currentcv = UserModels.query_current_user(request.email)
            if currentcv is not None and currentcv.get() is None:
                entity = UserModels.put_from_message(request)
                tokenEntity = UserToken.create(entity.key.id(), 'signup', None)
                return SignUpResponse(status='OK',userid=entity.key.id(), token=tokenEntity.token, fullname = entity.fullname)
            else:
                return SignUpResponse(status='email already exist')
        except (IndexError, TypeError):
            raise endpoints.NotFoundException('server error!')

    @endpoints.method(UserpostLogin, SignUpResponse,
                      path='account/login', http_method='POST',
                      name='account.login')
    def account_login(self, request):
        try:
            currentcv = UserModels.query_current_user(request.email)
            entity = currentcv.get()
            if currentcv is not None and entity is not None:
                if security.check_password_hash(request.password, currentcv.get().password):
                    currentusertoken = UserToken.query_current_user(entity.key.id())
                    tokenSaveEntity = currentusertoken.get()
                    if currentusertoken is not None and tokenSaveEntity is not None:
                        return SignUpResponse(status='OK',userid=entity.key.id(), token=tokenSaveEntity.token, fullname = entity.fullname)
                    else:
                        tokenNewEntity = UserToken.create(entity.key.id(), 'login', None)
                        return SignUpResponse(status='OK',userid=entity.key.id(), token=tokenNewEntity.token)
                else:
                    return SignUpResponse(status='password invalid')
            else:
                return SignUpResponse(status='email already exist')
        except (IndexError, TypeError):
            raise endpoints.NotFoundException('server error!')
    
    
    class UploadHandler(blobstore_handlers.BlobstoreUploadHandler):
        def post(self):
            logging.info('start upload handle')
            upload_files = self.get_uploads('file')  # 'file' is file upload field in the form
            blob_info = upload_files[0]
            self.redirect('/serve/%s' % blob_info.key())

    class ServeHandler(blobstore_handlers.BlobstoreDownloadHandler):
        def get(self, resource):
            resource = str(urllib.unquote(resource))
            blob_info = blobstore.BlobInfo.get(resource)
            self.send_blob(blob_info)

    ID_RESOURCE = endpoints.ResourceContainer(
            message_types.VoidMessage,
            userid=messages.IntegerField(1, variant=messages.Variant.INT64))

    @endpoints.method(ID_RESOURCE, URLResponse,
                      path='account/uploadimageurl/{userid}', http_method='GET',
                      name='uploadimageurl.getURL')
    def uploadimageurl_get(self, request):
        try:
            upload_url = blobstore.create_upload_url('/upload/image')
            app = webapp2.WSGIApplication([('/upload/image', UploadHandler)],
                              debug=True)
            return URLResponse(url=str(upload_url))
        except (IndexError, TypeError):
            raise endpoints.NotFoundException('server error!')

APPLICATION = endpoints.api_server([AccountApi])